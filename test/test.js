// require npm module html2js
// npm install karma-ng-html2js-preprocessor --save-dev
describe('Test Staff Search Directive: ', function() {
	beforeEach(module('MaqCodeTest'));
	var $scope, $compile, element;
	// using ngHtml2JsPreprocessor for external template url
	beforeEach(module('app/views/mqStaffSearch.html'));
	beforeEach(inject(function($rootScope, _$compile_) {
		$scope = $rootScope.$new();
		$compile = _$compile_;
		element = $compile('<mq-staff-search></mq-staff-search>')($scope);
		$scope.$digest();

	}));
	it('Sample staff data should exist', function() {
		var data = element.isolateScope().data;
		expect(data).not.to.equal(null);
		expect(data.length).to.equal(6);
	});
	it('Scope custom filter function filterStaffByName() should return true if search name match staff name', function() {
		var searchText = 'name:alex';
		var staff = { name: 'alex'};
		expect(element.isolateScope().filterStaffByName(searchText)(staff)).to.equal(true);
	});
	it('Scope custom filter function filterStaffByRole() should return true if search name match staff name', function() {
		var searchText = 'role:developer';
		var staff = { role: 'developer'};
		expect(element.isolateScope().filterStaffByRole(searchText)(staff)).to.equal(true);
	});
	it('Scope custom filter function filterStaffByOffice() should return true if search name match staff name', function() {
		var searchText = 'office:sydney';
		var staff = { office: 'sydney'};
		expect(element.isolateScope().filterStaffByOffice(searchText)(staff)).to.equal(true);
	});
	it('Scope function isMatchingStaff() should return true if text value contains the search text', function() {
		var isMatch = element.isolateScope().isMatchingStaff('name:alex', 'name', 'alex');
		expect(isMatch).to.equal(true);
	});
	it('Scope function isMatchingStaff() should return false if text value does not contain the search text', function() {
		var isMatch = element.isolateScope().isMatchingStaff('name:jerry', 'name', 'alex');
		expect(isMatch).to.equal(false);
	});
	it('Scope function getSearchParams() should return array of key value pair from search text', function() {
		var params = element.isolateScope().getSearchParams('name:alex role:developer office:sydney');
		expect(params.length).to.equal(3);
		params = element.isolateScope().getSearchParams('randomtext');
		expect(params.length).to.equal(0);
	});
});