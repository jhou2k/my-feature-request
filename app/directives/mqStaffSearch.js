angular.module('MaqCodeTest', [])
	.directive('mqStaffSearch', function() {
		return {
			restrict: 'E',
			templateUrl: 'app/views/mqStaffSearch.html',
			scope: {},
			link: function (scope, $filter) {
				// sample staff data
				scope.data = [
				        {
				            name: 'Alex',
				            role: 'Developer',
				            office: 'Sydney'
				        },
				        {
				            name: 'Ben',
				            role: 'Developer',
				            office: 'Wogga'
				        },
				        {
				            name: 'Sam',
				            role: 'Teacher',
				            office: 'Sydney'
				        },
				        {
				            name: 'Steve',
				            role: 'Builder',
				            office: 'Melbourne'
				        },
				        {
				            name: 'Josh',
				            role: 'Driver',
				            office: 'Sydney'
				        },
				        {
				            name: 'Sarah',
				            role: 'Lawyer',
				            office: 'Brisbane'
				        }
				    ];
				// custom filter function
				scope.filterStaffByName = function(searchText) {
					return function(staff) {
						return scope.isMatchingStaff(searchText, 'name', staff.name);
					};
				}
				scope.filterStaffByRole = function(searchText) {
					return function(staff) {
						return scope.isMatchingStaff(searchText, 'role', staff.role);
					};
				}
				scope.filterStaffByOffice = function(searchText) {
					return function(staff) {
						return scope.isMatchingStaff(searchText, 'office', staff.office);
					};
				}
				// get search text key value pairs
				scope.getSearchParams = function(searchText) {
					var searchParams = [];
					if (searchText) {
						var paramSeperator = ' ';
						var keyValuePairs = searchText.split(paramSeperator);
						var keyValueSeperator = ':';
						angular.forEach(keyValuePairs, function(keyValuePair){
							var keyValue = keyValuePair.split(keyValueSeperator);
							if (keyValue && keyValue.length == 2) {
								searchParams.push({ key: keyValue[0], value: keyValue[1] });
							}
						});
					}
					return searchParams;
				}
				// check all search params for matching key value
				scope.isMatchingStaff = function(searchText, key, value) {
					var searchParams = scope.getSearchParams(searchText);
					if (searchParams.length > 0) {
						for (var i=0; i<searchParams.length; i++) {
							if (searchParams[i].key.toLowerCase() === key) {
								// if param specified then try match text value
								var isMatch = (value.toLowerCase().indexOf(searchParams[i].value) > -1);
								return isMatch;
							}
						}
					}
					return true;
				}
			}
		};
});